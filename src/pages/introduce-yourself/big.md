---
title: บิ๊ก
slug: big
author: pongpat sitiputa
_nickname: บิีก
_role: guest
---

# 👋 Hi my name is Big.

My name is Pongpat Sitiputa. My nickname is Big.
I graduted from KMUTT, Computer Engineering.
I'm a Full stack Developer.

## ⚽️ Hobbies

- Youtube
- Play games

## 🍤 Favourite food

1. Noodles
2. ต้มยำกุ้ง

## 👨‍💻 Favourite programing language

- Javascript
- Python
