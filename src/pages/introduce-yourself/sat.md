---
title: แซท
slug: sat
author: Nakkrawee Kuenkleep
_nickname: แซท
_role: guest
---

# 👋 Hi my name is sat.

![alt text](../../images/profile.png)
- จบจากมหาวิทลัยเกษตรศาสตร์ สาขา วิทยาการคอมพิวเตอร์


## ⚽️ Hobbies

- Gamer
- Basketball 

## 🍤 Favourite food

1. กะเพราหมูสับ
2. หมาล่า (เผ็ดๆ)

## 👨‍💻 Favourite programing language

- Java
- C
