---
title: คิง
slug: king
author: ponpawit paoseng
_nickname: คิง
_role: guest
---

# 👋 Hi my name is King.

### My final project [TUVC](https://volunteer.sa.tu.ac.th/)

## ⚽️ Hobbies

- Anime
- Game
- Netflix

## 🍤 Favourite food

1. แกงเขียวหวาน
2. ผัดไทย
 etc.

## 👨‍💻 Favourite programing language

- Javascript
- Python

## Contact me

- Discord: KingOnHuy#5555
    - [My server](https://discord.gg/DynQgY7)
- Steam: [KingOnHuy](https://steamcommunity.com/id/KingOnHuy)

### I like Ina !!!!!!!!!!
![](https://cdn.discordapp.com/attachments/319130498792161280/851761698099822642/out-transparent-6.gif)