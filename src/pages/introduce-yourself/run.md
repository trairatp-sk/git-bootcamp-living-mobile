---
title: รัน
slug: run
author: Saran Tirattanasuwan
_nickname: รัน
_role: guest
---

# 👋 Hi my name is Run.

ชื่อรัน ชอบกินอันปัน แล้วก็ชอบกินของมันๆ ไม่ชอบแปรงฟัน ไม่ชอบกินอิชิตัน แต่ชอบเล่นเกมเหมือนกัน

## ⚽️ Hobbies

- Gaming
- Sleeping
- Eating

## 🍤 Favourite food

1. All

## 👨‍💻 Favourite programing language

- Javascript
- C++
