---
title: อาร์ม
slug: arm
author: ชูเกียรติ พัณพิบูลย์
_nickname: อาร์ม
_role: guest
---

# 👋 Hi my name is arm.

เป็นคนกาญจนบุรี
เข้ากับคนอื่นได้ง่าย
ชอบเรียนรู้

## ⚽️ Hobbies

- เล่นกีฬา
- ดูหนังผี

## 🍤 Favourite food

1. ก๋วยเตี๋ยวน้ำตก
2. ลาบ

## 👨‍💻 Favourite programing language

- python
- javascript
