---
title: ปลานิล
slug: planil
author: Prapasiri Sangthong
_nickname: ปลานิล
_role: guest
---

# 👋 Hi my name is Planil.
![alt text](../../images/planil.jpg) 

## ⚽️ Hobbies

- gel manicure
- DIY

## 🍤 Favourite food

1. ข้าวแกงกะหรี่
2. ข้าวซอยไก่

## 👨‍💻 Favourite programing language

- php
- C++
