---
title: ลีโอ
slug: leo
author: chayapol sivasettee
_nickname: ลีโอ
_role: guest
---

# 👋 Hello, my name is Chayapol.
Now, I'm working at Living mobile and I'm 24 years old.

## ⚽️ Hobbies

- Coding
- Gaming
- Sport

## 🍤 Favourite food

1. ข้าวหน้าเนื้อ
2. ต้มแซ่บกระดูกหมูอ่อน

## 👨‍💻 Favourite programing language

- Javascript
- Python

